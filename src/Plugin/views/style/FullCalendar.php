<?php

namespace Drupal\fullcalendar\Plugin\views\style;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\Annotation\ViewsStyle;
use Drupal\Core\Annotation\Translation;
use Drupal\fullcalendar\Plugin\FullcalendarPluginCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Entity\EntityViewDisplay;

/**
 * @todo.
 *
 * @ViewsStyle(
 *   id = "fullcalendar",
 *   title = @Translation("FullCalendar"),
 *   help = @Translation("Displays items on a calendar."),
 *   theme = "views_view_fullcalendar",
 *   display_types = {"normal"}
 * )
 */
class FullCalendar extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Stores the FullCalendar plugins used by this style plugin.
   *
   * @var \Drupal\fullcalendar\Plugin\FullcalendarPluginCollection;
   */
  protected $pluginBag;

  /**
   * {@inheritdoc}
   */
  public function evenEmpty() {
    return TRUE;
  }

  /**
   * @todo.
   *
   * @return \Drupal\fullcalendar\Plugin\FullcalendarPluginCollection;|\Drupal\fullcalendar\Plugin\FullcalendarInterface[]
   */
  public function getPlugins() {
    return $this->pluginBag;
  }

  /**
   * Constructs a new Fullcalendar object.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Component\Plugin\PluginManagerInterface $fullcalendar_manager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PluginManagerInterface $fullcalendar_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->pluginBag = new FullcalendarPluginCollection($fullcalendar_manager, $this);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.fullcalendar'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    /* @var \Drupal\fullcalendar\Plugin\fullcalendar\type\FullCalendar $plugin */
    $options = parent::defineOptions();
    foreach ($this->getPlugins() as $plugin) {
      $options += $plugin->defineOptions();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    /* @var \Drupal\fullcalendar\Plugin\fullcalendar\type\FullCalendar $plugin */
    parent::buildOptionsForm($form, $form_state);
    foreach ($this->getPlugins() as $plugin) {
      $plugin->buildOptionsForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    parent::validateOptionsForm($form, $form_state);

    // Cast all submitted values to their proper type.
    // @todo Remove once https://drupal.org/node/1653026 is in.
    if ($form_state->getValue('style_options')) {
      $this->castNestedValues($form_state->getValue('style_options'), $form);
    }
  }

  /**
   * Casts form values to a given type, if defined.
   *
   * @param array $values
   *   An array of fullcalendar option values.
   * @param array $form
   *   The fullcalendar option form definition.
   * @param string|null $current_key
   *   (optional) The current key being processed. Defaults to NULL.
   * @param array $parents
   *   (optional) An array of parent keys when recursing through the nested
   *   array. Defaults to an empty array.
   */
  protected function castNestedValues(array &$values, array $form, $current_key = NULL, array $parents = []) {
    foreach ($values as $key => &$value) {
      // We are leaving a recursive loop, remove the last parent key.
      if (empty($current_key)) {
        array_pop($parents);
      }

      // In case we recurse into an array, or need to specify the key for
      // drupal_array_get_nested_value(), add the current key to $parents.
      $parents[] = $key;

      if (is_array($value)) {
        // Enter another recursive loop.
        $this->castNestedValues($value, $form, $key, $parents);
      }
      else {
        // Get the form definition for this key.
        $form_value = NestedArray::getValue($form, $parents);
        // Check to see if #data_type is specified, if so, cast the value.
        if (isset($form_value['#data_type'])) {
          settype($value, $form_value['#data_type']);
        }
        // Remove the current key from $parents to move on to the next key.
        array_pop($parents);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    /* @var \Drupal\fullcalendar\Plugin\fullcalendar\type\FullCalendar $plugin */
    parent::submitOptionsForm($form, $form_state);
    foreach ($this->getPlugins() as $plugin) {
      $plugin->submitOptionsForm($form, $form_state);
    }
  }

  /**
   * @todo.
   */
  public function parseFields($include_gcal = TRUE) {
    $this->view->initHandlers();
    $labels = $this->displayHandler->getFieldLabels();
    $date_fields = [];
    foreach ($this->view->field as $id => $field) {
      if (fullcalendar_field_is_date($field, $include_gcal)) {
        $date_fields[$id] = $labels[$id];
      }
    }
    return $date_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if ($this->displayHandler->display['display_plugin'] != 'default' && !$this->parseFields()) {
      drupal_set_message($this->t('Display "@display" requires at least one date field.', ['@display' => $this->displayHandler->display['display_title']]), 'error');
    }
    return parent::validate();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#attached' => $this->prepareAttached(),
    ];
  }

  /**
   * Load libraries.
   */
  protected function prepareAttached() {
    /* @var \Drupal\fullcalendar\Plugin\fullcalendar\type\FullCalendar $plugin */
    $attached['library'][] = 'fullcalendar/drupal.fullcalendar';

    foreach ($this->getPlugins() as $plugin_id => $plugin) {
      $definition = $plugin->getPluginDefinition();
      foreach (['css', 'js'] as $type) {
        if ($definition[$type]) {
          $attached['library'][] = 'fullcalendar/drupal.fullcalendar.' . $type;
        }
      }
    }

    if ($this->displayHandler->getOption('use_ajax')) {
      $attached['library'][] = 'fullcalendar/drupal.fullcalendar.ajax';
    }

    $attached['drupalSettings']['fullcalendar'] = ['.js-view-dom-id-' . $this->view->dom_id => $this->prepareSettings()];

    return $attached;
  }

  /**
   * @todo.
   */
  protected function prepareSettings() {
    /* @var \Drupal\fullcalendar\Plugin\fullcalendar\type\FullCalendar $plugin */
    $settings = [];
    $weights = [];
    $delta = 0;
    foreach ($this->getPlugins() as $plugin_id => $plugin) {
      $definition = $plugin->getPluginDefinition();
      $plugin->process($settings);
      if (isset($definition['weight']) && !isset($weights[$definition['weight']])) {
        $weights[$definition['weight']] = $plugin_id;
      }
      else {
        while (isset($weights[$delta])) {
          $delta++;
        }
        $weights[$delta] = $plugin_id;
      }
    }
    ksort($weights);
    $settings['weights'] = array_values($weights);
    // @todo.
    $settings['fullcalendar']['disableResizing'] = TRUE;
    $settings['events'] = $this->prepareEvents();
    return $settings;
  }

  /**
   * Gets the date fields.
   *
   * @return \Drupal\views\Plugin\views\field\EntityField[]
   *   Date start and date end fields
   */
  protected function getDateFields() {
    $date_start = $date_end = NULL;

    // If using a custom date field, filter the fields to process.
    if (!empty($this->options['fields']['date_field_from']) && isset($this->view->field[$this->options['fields']['date_field_from']])) {
      $date_start = $date_end = $this->view->field[$this->options['fields']['date_field_from']];
    }
    if (!empty($this->options['fields']['date_field_to']) && isset($this->view->field[$this->options['fields']['date_field_to']])) {
      $date_end = $this->view->field[$this->options['fields']['date_field_to']];
    }

    return [$date_start, $date_end];
  }

  /**
   * Get event classes.
   *
   * @param $entity
   *   Entity object.
   *
   * @return array
   *   Entity classes.
   */
  protected function eventClasses($entity) {
    $classes = $this->moduleHandler->invokeAll('fullcalendar_classes', [$entity]);
    $this->moduleHandler->alter('fullcalendar_classes', $classes, $entity);
    return array_map(['\Drupal\Component\Utility\Html', 'getClass'], $classes);
  }

  /**
   * @todo.
   */
  protected function prepareEvents() {
    list($date_start, $date_end) = $this->getDateFields();
    $use_end_value = ($date_end === $date_start);
    if (empty($date_start)) {
      return [];
    }
    $events = [];
    // Get field type only for start as they should be the same.
    $reflection = new \ReflectionClass($date_start);
    $property = $reflection->getProperty('fieldDefinition');
    $property->setAccessible(TRUE);
    $type = $property->getValue($date_start)->getType();
    foreach ($this->view->result as $delta => $row) {
      $start = $this->parseDate($date_start->getValue($row), $type);
      if ($date_end) {
        $end = $this->parseDate($date_end->getValue($row, $use_end_value ? 'value_end' : NULL), $type);
      }
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $row->_entity;
      $classes = $this->eventClasses($entity);

      $all_day = FALSE;
      // Add a class if the event was in the past or is in the future, based
      // on the end time. We can't do this in hook_fullcalendar_classes()
      // because the date hasn't been processed yet.
      // todo: get rid of this one.
      if (($all_day && strtotime($start) < strtotime('today')) || (!$all_day && strtotime($end) < REQUEST_TIME)) {
        $time_class = 'fc-event-past';
      }
      elseif (strtotime($start) > REQUEST_TIME) {
        $time_class = 'fc-event-future';
      }
      else {
        $time_class = 'fc-event-now';
      }

      $events[] = [
        'eid' => $entity->id(),
        'entity_type' => $entity->getEntityTypeId(),
        'title' => strip_tags(htmlspecialchars_decode($entity->label(), ENT_QUOTES)),
        'start' => $start,
        'end' => $end,
        'url' => $entity->toUrl()->toString(),
        'allDay' => $all_day,
        'className' => implode(' ', $classes + [$time_class]),
        'editable' => FALSE,
        'dom_id' => $this->view->dom_id,
      ];
    }

    return $events;
  }

  /**
   * Parse date field.
   *
   * @param string $value
   *
   * @param string $type
   *
   * @return string
   *   Parsed date in rfc3339 format or default.
   */
  protected function parseDate($value, $type) {
    if (in_array($type, ['timestamp', 'created', 'changed'])) {
      return date(\DateTime::RFC3339, $value);
    }
    return $value;
  }

}
